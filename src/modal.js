import React from 'react';
import { CSSTransition } from "react-transition-group";
import "./modal.css";

const Modal = (props) => {
  return (
    <CSSTransition
      in={props.show}
      unmountOnExit
      classNames="modal"
      timeout={{ enter: 500, exit: 750}}
    >
      <div className="modal" onClick={props.onClose}>
        <div onClick={e => e.stopPropagation()}> {/* stopPropagation prevents onClose event from closing modal when clicking inside modal content. Useful for modals that have form fields */}
          <p className="close" onClick={props.onClose}>X</p>
          <img src={props.img} alt="" />
        </div>
      </div>
    </CSSTransition>
  );
};

export default Modal;