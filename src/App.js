import image1 from "./assets/city-skyline.jpg";
import image2 from "./assets/old-city-hall.jpg";
import image3 from "./assets/cn-tower.jpg";
import image4 from "./assets/rom.jpg";
import image5 from "./assets/rogers-center.jpg";
import image6 from "./assets/new-city-hall.jpg";
import Modal from "./modal";
import './App.css';
import React, { useState } from 'react';

const App = () => {
  const [show1, setShow1] = useState(false);
  const [show2, setShow2] = useState(false);
  const [show3, setShow3] = useState(false);
  const [show4, setShow4] = useState(false);
  const [show5, setShow5] = useState(false);
  const [show6, setShow6] = useState(false);

  return (
    <div className="App">
      <header className="App-header">
        <p>The City of <span className="bold">Toronto</span></p>
        <p className="App-sub-head">Historical Sites & <span className="bold">More</span></p>
      </header>
      <section className="image-wrapper">
        <div className="image-container"><img src={image1} alt="Toronto Skyline" />
          <div className="overlay" onClick={() => setShow1(true)}>
            <p className="bold">Toronto</p>
            <p>The Skyline</p>
          </div>
        </div>
        <div className="image-container"><img src={image2} alt="Toronto Old City Hall" />
          <div className="overlay" onClick={() => setShow2(true)}>
            <p className="bold">Old City Hall</p>
            <p>The Clock Tower</p>
          </div>
        </div>
        <div className="image-container"><img src={image3} alt="CN Tower" />
          <div className="overlay" onClick={() => setShow3(true)}>
            <p className="bold">The CN Tower</p>
            <p>One of the World's Tallest Towers</p>
          </div>
        </div>
        <div className="image-container"><img src={image4} alt="Royal Ontario Museum" />
          <div className="overlay" onClick={() => setShow4(true)}>
            <p className="bold">Royal Ontario Museum</p>
            <p>The Most Visited Museum in Canada</p>
          </div>
        </div>
        <div className="image-container"><img src={image5} alt="Rogers Centre Skydome" />
          <div className="overlay" onClick={() => setShow5(true)}>
            <p className="bold">The Rogers Centre</p>
            <p>Toronto's Iconic Skydome</p>
          </div>
        </div>
        <div className="image-container"><img src={image6} alt="Toronto New City Hall" />
          <div className="overlay" onClick={() => setShow6(true)}>
            <p className="bold">Toronto City Hall</p>
            <p>A Brutalist Architecture Masterpiece</p>
          </div>
        </div>
        <div>
          <Modal show={show1} onClose={() => setShow1(false)}
          img = {image1} alt="Toronto Skyline"
          />
          <Modal show={show2} onClose={() => setShow2(false)}
          img = {image2} alt="Toronto Old City Hall"
          />
          <Modal show={show3} onClose={() => setShow3(false)}
          img = {image3} alt="CN Tower"
          />
          <Modal show={show4} onClose={() => setShow4(false)}
          img = {image4} alt="Royal Ontario Museum"
          />
          <Modal show={show5} onClose={() => setShow5(false)}
          img = {image5} alt="Rogers Centre Skydome"
          />
          <Modal show={show6} onClose={() => setShow6(false)}
          img = {image6} alt="Toronto New City Hall"
          />
        </div>
      </section>
      <footer>
        <ul class="icons">
          <li><a href="https://twitter.com/cityoftoronto?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor" target="_blank" rel="noreferrer"><i class="fa-brands fa-twitter"></i></a></li>

          <li><a href="https://www.facebook.com/cityofto" target="_blank" rel="noreferrer"><i class="fa-brands fa-facebook-f"></i></a></li>

          <li><a href="https://www.instagram.com/cityofto/?hl=en" target="_blank" rel="noreferrer"><i class="fa-brands fa-instagram"></i></a></li>

          <li><a href="https://ca.linkedin.com/company/city-of-toronto" target="_blank" rel="noreferrer"><i class="fa-brands fa-linkedin-in"></i></a></li>
        </ul>
      </footer>
    </div>
  );
};

export default App;
